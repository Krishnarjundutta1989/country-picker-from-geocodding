//
//  ViewController.m
//  LocatonSearchInMap
//
//  Created by click labs 115 on 10/14/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    CLLocationManager *manager;
    CLLocation *currentLocation;
    NSString *city;
    NSString *state;
    CLLocationCoordinate2D position;
    GMSMarker *marker;
    NSString *Country;
    NSString *URL;
    NSString *address;
    NSMutableArray *arrayForFormattedAddress;
    UITableViewCell *cell;
}
@property (strong, nonatomic) IBOutlet UISearchBar *searchCity;
@property (strong, nonatomic) IBOutlet UITableView *tblForFormattedAddress;

@end

@implementation ViewController
@synthesize searchCity;
@synthesize tblForFormattedAddress;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self getaddress ];
    arrayForFormattedAddress = [NSMutableArray new];
    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [manager startUpdatingLocation];
    
    tblForFormattedAddress.hidden = YES;
    self.appleMap.delegate = self;
    self.googleMap.delegate = self;
    searchCity.delegate = self;
    
    [manager requestWhenInUseAuthorization];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    
    if (currentLocation != nil) {
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate =  currentLocation.coordinate;
        point.title = [NSString stringWithFormat:@"%@",city];
        point.subtitle = @"I'm here!!!";
        
        MKMapCamera *newCamera = [MKMapCamera cameraLookingAtCenterCoordinate:currentLocation.coordinate
                                                            fromEyeCoordinate:currentLocation.coordinate
                                                                  eyeAltitude:50];
        [newCamera setHeading:100.0]; // or newCamera.heading + 90.0 % 360.0
        [newCamera setPitch:45.0];
        //[newCamera setHeading:90.0];
        // [newCamera setAltitude:500.0];
        [_appleMap setCamera:newCamera animated:YES];
        
        [self.appleMap addAnnotation:point];
        
        
        position = currentLocation.coordinate;
        //GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
        // longitude:currentLocation.coordinate.longitude
        //    zoom:10.0];
        //  [_googleMap animateToCameraPosition:camera];
        [self getaddress];
        
        
    }
    
    
}
-(void) getaddress  {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude
                                                        longitude:currentLocation.coordinate.longitude];
    
    [geocoder reverseGeocodeLocation:newLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       if (error) {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       
                       if (placemarks && placemarks.count > 0)
                       {
                           CLPlacemark *placemark = placemarks[0];
                           
                           NSDictionary *addressDictionary =
                           placemark.addressDictionary;
                           
                           NSLog(@"%@ ", addressDictionary);
                           city = addressDictionary[@"City"];
                           NSLog(@"%@",city);
                           
                           
                           
                       }
                       
                       
                   }];
    
}
- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    position = coordinate;
    
    // GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude
    //longitude:coordinate.longitude
    //  zoom:2.0];
    //  [_googleMap animateToCameraPosition:camera];
    marker = [GMSMarker markerWithPosition:position];
    [self CilkAndMark];
    
    
    
    
}

-(void) CilkAndMark  {
    CLGeocoder *geocoder2 = [[CLGeocoder alloc] init];
    
    CLLocation *newLocation2 = [[CLLocation alloc]initWithLatitude:position.latitude
                                                         longitude:position.longitude];
    
    [geocoder2 reverseGeocodeLocation:newLocation2
                    completionHandler:^(NSArray *placemarks, NSError *error) {
                        
                        if (error) {
                            NSLog(@"Geocode failed with error: %@", error);
                            return;
                        }
                        
                        if (placemarks && placemarks.count > 0)
                        {
                            CLPlacemark *placemark = placemarks[0];
                            
                            NSDictionary *addressDictionary =
                            placemark.addressDictionary;
                            
                            NSLog(@"%@ ", addressDictionary);
                            if (addressDictionary[@"City"] != NULL ){
                                city = addressDictionary[@"City"];
                                NSLog(@"%@",city);
                                marker = [GMSMarker markerWithPosition:position];
                                marker.title = [NSString stringWithFormat:@"%@",city];
                                
                                marker.map = _googleMap;
                                
                            }
                            else if ((addressDictionary[@"State"] != NULL )){
                                state = addressDictionary[@"State"];
                                NSLog(@"%@",state);
                                marker = [GMSMarker markerWithPosition:position];
                                marker.title = [NSString stringWithFormat:@"%@",state];
                                
                                marker.map = _googleMap;
                                
                            }
                            
                            else if ((addressDictionary[@"Country"] != NULL )){
                                Country = addressDictionary[@"Country"];
                                NSLog(@"%@",Country);
                                marker = [GMSMarker markerWithPosition:position];
                                marker.title = [NSString stringWithFormat:@"%@",Country];
                                
                                marker.map = _googleMap;
                                
                            }
                        }
                        
                        
                    }];
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    address = searchCity.text;
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (CFStringRef)address,
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8 ));
       URL = [NSString
           stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&key=AIzaSyBhCly2RqKlh8c0-UmrpIPzTwsXpu33ojo",encodedString];
    NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL]];
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSArray *searchAddress = json[@"results"];
    for (int i=0; i<searchAddress.count; i++) {
        NSDictionary *searchFound = searchAddress[i];
        if ((searchFound[@"geometry"] != NULL)&&(searchFound[@"formatted_address"] != NULL)) {
            NSDictionary *Geometry = searchFound[@"geometry"];
            NSDictionary *location = Geometry[@"location"];
            NSString *lat = location[@"lat"];
            NSString *lng = location[@"lng"];
            address = searchFound[@"formatted_address"];
            position = CLLocationCoordinate2DMake(lat.doubleValue,lng.doubleValue);
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:position.latitude
                                                                    longitude:position.longitude
                                                                         zoom:10.0];
            [_googleMap animateToCameraPosition:camera];
            marker = [GMSMarker markerWithPosition:position];
            marker.title = [NSString stringWithFormat:@"%@",address];
            marker.map = _googleMap;
            [arrayForFormattedAddress addObject:address];
            
        }
        
    }
    [tblForFormattedAddress reloadData];
    
}
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
    
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayForFormattedAddress.count;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    cell = [tableView dequeueReusableCellWithIdentifier:@"reuse"];
    cell.textLabel.text = arrayForFormattedAddress [indexPath.row];
    tblForFormattedAddress.hidden = NO;
    
    return cell;
}

//- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
//
//    tblForFormattedAddress.hidden = NO;
//    return YES;
//}


-(void)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Space Not Allow Only Comma Allow" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *formattedAdress = [NSString stringWithFormat:@"%@",cell.textLabel.text];
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (CFStringRef)formattedAdress,
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8 ));
    URL = [NSString
           stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&key=AIzaSyBhCly2RqKlh8c0-UmrpIPzTwsXpu33ojo",encodedString];
    NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL]];
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSArray *searchAddress = json[@"results"];
    for (int i=0; i<searchAddress.count; i++) {
        NSDictionary *searchFound = searchAddress[i];
        if ((searchFound[@"geometry"] != NULL)&&(searchFound[@"formatted_address"] != NULL)) {
            NSDictionary *Geometry = searchFound[@"geometry"];
            NSDictionary *location = Geometry[@"location"];
            NSString *lat = location[@"lat"];
            NSString *lng = location[@"lng"];
            address = searchFound[@"formatted_address"];
            position = CLLocationCoordinate2DMake(lat.doubleValue,lng.doubleValue);
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:position.latitude
                                                                    longitude:position.longitude
                                                                         zoom:10.0];
            [_googleMap animateToCameraPosition:camera];
            marker = [GMSMarker markerWithPosition:position];
            marker.title = [NSString stringWithFormat:@"%@",address];
            marker.map = _googleMap;
            
            break;
            
            
            
            
        }
        
    }
    tblForFormattedAddress.hidden = TRUE;
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row %2) {
        [cell setBackgroundColor:[UIColor grayColor]] ;
    }
    else{
        [cell setBackgroundColor:[UIColor lightGrayColor]];
    }
}

- (void) didPan:(UIPanGestureRecognizer*) gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        _googleMap.settings.scrollGestures = true;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (gestureRecognizer.numberOfTouches > 1)
    {
        _googleMap.settings.scrollGestures = false;
    }
    else
    {
        _googleMap.settings.scrollGestures = true;
    }
    return true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
